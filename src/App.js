import React, { Component } from 'react';
import { Widget, addResponseMessage } from 'react-chat-widget';

class App extends Component {
  componentDidMount() {
    addResponseMessage("Tjenare");
  }

  handleNewUserMessage = (newMessage) => {
    console.log(`New message incomig! ${newMessage}`);
    // Now send the message throught the backend API


    addResponseMessage("Svar från Watson");
  }

  render() {
    return (
        <div className="App">
          <Widget
              handleNewUserMessage={this.handleNewUserMessage}

              title="Colburn"
              subtitle="Watson chat"
          />
        </div>
    );
  }
}

export default App;